<?php

namespace App\Transport;

use App\Driver\AbstractDriver;
use App\Route\RouteInterface;

/**
 * Class UrbanTransport
 * @package App
 */
abstract class UrbanTransport
{
    /**
     * @var RouteInterface;
     */
    protected $route;

    /**
     * @var RouteInterface;
     */
    protected $driver;

    /**
     * @var int
     */
    protected $numberOfSeats;

    /**
     * @var int
     */
    protected $numberOfSeatsTaken = 0;

    /**
     * @var float
     */
    protected $fare;

    /**
     * @var int
     */
    protected $last_enteder = 0;

    /**
     * @var int
     */
    protected $last_leaved = 0;

    /**
     * UrbanTransport constructor.
     * @param RouteInterface $route
     * @param int $numberOfSeats
     * @param AbstractDriver $driver
     * @param float $fare
     */
    public function __construct(RouteInterface $route, int $numberOfSeats, AbstractDriver $driver, float $fare)
    {
        $this->route = $route;
        if (empty($numberOfSeats)) {
            throw new \InvalidArgumentException('Number of seats must be > 0');
        }
        $this->numberOfSeats = $numberOfSeats;
        if (empty($fare)) {
            throw new \InvalidArgumentException('Fare must be > 0');
        }
        $this->driver = $driver;
        $this->fare = $fare;
    }

    /**
     * @return string
     */
    abstract public function startRide(): string;

    /**
     * @return string
     */
    abstract public function stopRide(): string;

    /**
     * @return string
     */
    abstract public function openDoors(): string;

    /**
     * @return string
     */
    abstract public function closeDoors(): string;

    /**
     * @return bool
     */
    public function isFull(): bool
    {
        return $this->numberOfSeats === $this->numberOfSeatsTaken;
    }

    /**
     * @return int
     */
    public function getFreeSeats(): int
    {
        return $this->numberOfSeats - $this->numberOfSeatsTaken;
    }

    /**
     * @return int
     */
    public function getNumberOfSeatsTaken(): int
    {
        return $this->numberOfSeatsTaken;
    }

    /**
     * @param int $number
     */
    public function addPassangers(int $number): void
    {
        if ($number <= $this->getFreeSeats()) {
            $this->last_enteder = $number;
        } else {
            $this->last_enteder = $this->getFreeSeats();
        }

        $this->numberOfSeatsTaken += $this->last_enteder;
    }

    /**
     * @param int $number
     */
    public function removePassangers(int $number): void
    {
        if ($number <= $this->numberOfSeatsTaken) {
            $this->last_leaved = $number;
        } else {
            $this->last_leaved = $this->numberOfSeatsTaken;
        }

        $this->numberOfSeatsTaken -= $this->last_leaved;
    }

}