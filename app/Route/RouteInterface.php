<?php



namespace App\Route;

/**
 * Interface RouteInterface
 * @package App\Route
 */
interface RouteInterface
{
    /**
     * @return void
     */
    public function initPath(): void;

    /**
     * @return array
     */
    public function getPath(): array;

    /**
     * @return void
     */
    public function nextStation(): void;

    /**
     * @return boolean
     */
    public function isRouteEnd(): bool;
}