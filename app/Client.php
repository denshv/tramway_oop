<?php

declare(strict_types=1);

namespace App;

use App\Transport\Tram;
use App\Route\TramRoute;
use App\Driver\TramDriver;

/**
 * Class Client
 * @package App
 */
class Client
{
    /**
     * @var TramRoute
     */
    private $route;

    /**
     * @var Tram
     */
    private $tram;

    /**
     * @var TramDriver
     */
    private $driver;

    /**
     * Client constructor.
     */
    public function __construct()
    {
        try {
            $this->route = new TramRoute(5);
            $this->route->initPath();
        } catch (\Exception $e) {
            die($e->getMessage());
        }

        $this->driver = new TramDriver('Ivan','ABC 123');
        $this->tram = new Tram($this->route, 45, $this->driver, 7);
    }

    /**
     * Function run client code
     */
    public function run():void
    {
        while (!$this->route->isRouteEnd()){

            var_dump($this->route->getCurrentStation());
            $this->tram->startRide();
            $this->tram->stopRide();
            $this->tram->openDoors();
            $this->tram->closeDoors();
            $this->tram->startRide();

            $this->route->nextStation();
        }
    }
}