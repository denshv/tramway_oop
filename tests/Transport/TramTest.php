<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use App\Transport\Tram;
use App\Route\TramRoute;
use App\Driver\TramDriver;

/**
 * Class TramTest
 */
class TramTest extends TestCase
{
    /**
     * @var  Tram
     */
    protected $tram;

    /**
     * @var TramRoute
     */
    protected $route;

    /**
     * @var TramDriver
     */
    private $driver;

    /**
     * @var int
     */
    protected $numberOfSeats = 45;

    /**
     * @throws Exception
     */
    public function setUp(): void
    {
        $this->route = $this->createMock(TramRoute::class);
        $this->driver = $this->createMock(TramDriver::class);

        $this->tram = new Tram($this->route, $this->numberOfSeats, $this->driver, 7);
    }

    public function testStartRide(): void
    {
        $this->assertEquals($this->tram->startRide(), 'Tram start');
    }

    public function testStopRide(): void
    {
        $this->assertEquals($this->tram->stopRide(), 'Tram stop');
    }

    public function testOpenDoors(): void
    {
        $this->assertEquals($this->tram->openDoors(), 'Tram open doors');
        $taken = $this->tram->getNumberOfSeatsTaken();
        $this->assertTrue($this->numberOfSeats >= $taken);
    }

    public function testCloseDoors(): void
    {
        $this->assertEquals($this->tram->closeDoors(), 'Tram close doors');
    }

    /**
     * @dataProvider passangerAddProviderSuccess
     */
    public function testAddPassangersSuccess($a, $b)
    {
        $this->tram->addPassangers($a);
        $seats = $this->tram->getNumberOfSeatsTaken();

        $this->assertEquals($seats, $b);
    }

    /**
     * @dataProvider passangerAddProviderFails
     */
    public function testAddPassangersFails($a, $b)
    {
        $this->tram->addPassangers($a);
        $seats = $this->tram->getNumberOfSeatsTaken();

        $this->assertNotEquals($seats, $b);
    }

    /**
     * @dataProvider passangerProviderException
     */
    public function testAddPassangersException($a, $exception)
    {
        $this->expectException($exception);
        $this->tram->addPassangers($a);
    }


    /**
     * @dataProvider passangerRemoveProviderSuccess
     */
    public function testRemovePassangersSuccess($a, $b)
    {
        $this->tram->addPassangers($a);
        $this->tram->removePassangers($a);
        $seats = $this->tram->getNumberOfSeatsTaken();

        $this->assertEquals($seats, $b);
    }

    /**
     * @dataProvider passangerRemoveProviderFails
     */
    public function testRemovePassangersFails($a, $b)
    {
        $this->tram->addPassangers($a);
        $this->tram->removePassangers($a);
        $seats = $this->tram->getNumberOfSeatsTaken();

        $this->assertNotEquals($seats, $b);
    }

    /**
     * @dataProvider passangerProviderException
     */
    public function testRemovePassangersException($a, $exception)
    {
        $this->expectException($exception);
        $this->tram->removePassangers($a);
    }


    /**** Data providers ****/

    /**
     * @return array
     */
    public function passangerAddProviderSuccess()
    {
        return [
            [11, 11],
            [100, $this->numberOfSeats],
            [0, 0],

        ];
    }

    /**
     * @return array
     */
    public function passangerAddProviderFails()
    {
        return [
            [11, 50],
            [100, 100],
        ];
    }

    /**
     * @return array
     */
    public function passangerRemoveProviderSuccess()
    {
        return [
            [11, 0],
            [$this->numberOfSeats, 0],
            [100, 0],
            [0, 0],
        ];
    }

    /**
     * @return array
     */
    public function passangerRemoveProviderFails()
    {
        return [
            [11, 50],
            [100, 100],
        ];
    }

    /**
     * @return array
     */
    public function passangerProviderException()
    {
        return [
            [true, \TypeError::class],
            ['str', \TypeError::class],
            [[], \TypeError::class],
        ];
    }

}