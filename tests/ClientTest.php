<?php
declare(strict_types=1);

namespace tests;

require_once __DIR__ . '/../config/main.php';

use PHPUnit\Framework\TestCase;
use App\Transport\Tram;
use App\Route\TramRoute;

/**
 * Class ClientTest
 * @package tests
 */
class ClientTest extends TestCase
{
    /**
     * @var TramRoute
     */
    private $routeStub;

    /**
     * @var Tram
     */
    private $tramStub;

    private $tram_status_map = [
        'startRide' => 'Tram start',
        'stopRide' => 'Tram stop',
        'openDoors' => 'Tram open doors',
        'closeDoors' => 'Tram close doors',
    ];

    /**
     * @throws \ReflectionException
     */
    public function setUp(): void
    {

        $this->routeStub = $this->getMockBuilder(TramRoute::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'initPath', 'getPath', 'isRouteEnd', 'nextStation'])->getMock();

        $this->tramStub = $this->getMockBuilder(Tram::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'startRide', 'stopRide', 'openDoors', 'closeDoors'
            ])->getMock();


        foreach ($this->tram_status_map as $method => $returnVal) {
            $this->tramStub->method($method)
                ->willReturn($returnVal);
        }

    }


    /**
     * @throws \Exception
     */
    public function testRunSuccees()
    {
        $map = json_decode(file_get_contents(TramRoute::PATH_TO_ROUTES . 28 . '.json'));

        $this->routeStub->method('getPath')
            ->will($this->returnValue($map));

        $path = $this->routeStub->getPath();

        $number_of_stations = count($path);

        $this->routeStub->expects($this->exactly($number_of_stations))->method('nextStation');

        $i = 0;
        while (!$this->routeStub->isRouteEnd()) {

            $s = $this->tramStub->startRide();
            $this->assertEquals($this->tram_status_map['startRide'], $s);
            $s = $this->tramStub->stopRide();
            $this->assertEquals($this->tram_status_map['stopRide'], $s);
            $s = $this->tramStub->openDoors();
            $this->assertEquals($this->tram_status_map['openDoors'], $s);
            $s = $this->tramStub->closeDoors();
            $this->assertEquals($this->tram_status_map['closeDoors'], $s);
            $s = $this->tramStub->startRide();
            $this->assertEquals($this->tram_status_map['startRide'], $s);
            $this->routeStub->nextStation();

            if ($i == $number_of_stations - 1) {
                $this->routeStub->method('isRouteEnd')
                    ->will($this->returnValue(true));
            }

            $i++;
        }

    }

}