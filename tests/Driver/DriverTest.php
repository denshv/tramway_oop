<?php

declare(strict_types=1);

use App\Driver\TramDriver;

class DriverTest extends \PHPUnit\Framework\TestCase
{

    protected $driver;

    public function testConstuctSuccess()
    {
        $this->driver = new TramDriver('test', 'abc 23');
        $this->assertIsObject($this->driver);
    }

    /**
     * @dataProvider driverProviderException
     * @param $a
     * @param $b
     * @param $exception
     * @throws Exception
     */
    public function testConstructFails($a, $b, $exception)
    {
        $this->expectException($exception);
        $this->driver = new TramDriver($a, $b);
    }

    /**
     * @return array
     */
    public function driverProviderException()
    {
        return [
            [true, true, \TypeError::class],
            [123, 134, \TypeError::class],
            [[], true, \TypeError::class],
        ];
    }
}