<?php

namespace App\Transport;

class Tram extends UrbanTransport
{

    public function startRide(): string
    {
        return 'Tram start';
    }

    public function stopRide(): string
    {
        return 'Tram stop';
    }

    public function openDoors(): string
    {
        $this->addPassangers(rand(0, $this->getFreeSeats()));
        $this->removePassangers(rand(0, $this->numberOfSeatsTaken));
        return 'Tram open doors';
    }

    public function closeDoors(): string
    {
        return 'Tram close doors';
    }
}