<?php



namespace App\Driver;

/**
 * Class AbstractDriver
 * @package App\Driver
 */
abstract class AbstractDriver
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $licence;

    /**
     * AbstractDriver constructor.
     * @param $name
     * @param $licence
     */
    public function __construct(string $name, string $licence)
    {
        $this->name = $name;
        $this->licence = $licence;
    }
}