<?php

declare(strict_types=1);

require_once __DIR__ . '/../../config/main.php';

use App\Route\TramRoute;

class RouteTest extends \PHPUnit\Framework\TestCase
{

    /**
     * @var TramRoute
     */
    protected $route;

    protected $valid_number = 28;

    protected $not_valid_numbers = [30, 33];

    /**
     * @throws Exception
     */
    public function setUp(): void
    {
        $this->route = new TramRoute($this->valid_number);
    }

    /**
     * @dataProvider constructProviderException
     * @param $a
     * @param $exception
     * @throws Exception
     */
    public function testConstructorException($a, $exception)
    {
        $this->expectException($exception);
        $this->route = new TramRoute($a);
    }

    /**
     * @return array
     */
    public function constructProviderException()
    {
        return [
            [true, TypeError::class],
            ['str', TypeError::class],
            [[], TypeError::class],
        ];
    }

    /**
     * @dataProvider initProviderException
     * @param $a
     * @param $exception
     * @throws Exception
     */
    public function testInitPathFails($a, $exception)
    {
        $this->expectException($exception);
        $this->route = new TramRoute($a);
        $this->route->initPath();
    }

    /**
     * @return array
     */
    public function initProviderException()
    {
        return [
            [true, TypeError::class],
            ['str', TypeError::class],
            [[], TypeError::class],
            [$this->not_valid_numbers[0], \App\Exceptions\FileNotReadableException::class],
            [$this->not_valid_numbers[1], \Exception::class],
            [4545, \App\Exceptions\FileNotFoundException::class],
            [[], TypeError::class],
        ];
    }

    /**
     * @throws Exception
     */
    public function testGetPathSuccess()
    {
        $this->route->initPath();
        $path = $this->route->getPath();
        $this->assertIsArray($path);
        $this->assertGreaterThan(0, count($path));

        return $this->route;
    }

    /**
     * @throws Exception
     */
    public function testGetPathFails()
    {
        $this->expectException(InvalidArgumentException::class);
        $this->route->getPath();
    }

    /**
     * @depends testGetPathSuccess
     *
     * @param TramRoute $route
     * @throws Exception
     */
    public function testGetCurrentStationSuccess(TramRoute $route)
    {
        $station = $route->getCurrentStation();
        $this->assertIsString($station);
    }

    public function testGetCurrentStationFails()
    {
        $this->expectException(InvalidArgumentException::class);
        $station = $this->route->getCurrentStation();
        $this->assertIsNotString($station);
    }


    /**
     * @depends testGetPathSuccess
     *
     * @param TramRoute $route
     * @throws Exception
     */
    public function testIsRouteEndSuccess(TramRoute $route)
    {
        $path = $route->getPath();

        foreach ($path as $station) {
            $route->nextStation();
        }

        $this->assertTrue($route->isRouteEnd());
    }

    /**
     * @depends testGetPathSuccess
     *
     * @throws Exception
     */
    public function testIsRouteEndFails()
    {
        $this->route->initPath();
        $this->assertFalse($this->route->isRouteEnd());
    }
}