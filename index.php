<?php

require_once 'config/main.php';
require 'vendor/autoload.php';
//require_once 'autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', true);

$client = new \App\Client();
$client->run();
