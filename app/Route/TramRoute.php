<?php



namespace App\Route;

use App\Exceptions\FileNotFoundException;
use App\Exceptions\FileNotReadableException;

require_once __DIR__ . '/../../config/main.php';


/**
 * Class TramRoute
 * @package App
 */
class TramRoute implements RouteInterface
{
    const PATH_TO_ROUTES = BASE_PATH . '/routes/';

    const DEFAULT_DIRECTION = 0;

    /**
     * @var int
     */
    private $number;

    /**
     * @var []
     */
    private $list;

    /**
     * @var int
     */
    private $current_pos = 0;

    /**
     * TramRoute constructor.
     * @param int $number
     * @throws \Exception
     */
    public function __construct(int $number)
    {
        if (empty($number)) {
            throw  new \InvalidArgumentException('Route number not set');
        }
        $this->number = $number;
    }

    /**
     * @throws \Exception
     */
    public function initPath():void
    {
        $filePath = self::PATH_TO_ROUTES . $this->number . '.json';
        if (!file_exists($filePath)) {
            throw  new FileNotFoundException('Route list not found');
        }

        $routes = file_get_contents($filePath);
        if (empty($routes)) {
            throw  new \Exception('Route list not found');
        }

        $routes_list = json_decode($routes, true);

        if (empty($routes_list)) {
            throw  new FileNotReadableException('Route number not found in route list');
        }

        $this->list = $routes_list;
        $this->current_pos = 0;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getPath():array
    {
        if (empty($this->list)) {
            throw  new \InvalidArgumentException('Route path not found');
        }

        return $this->list;
    }

    public function getCurrentStation():string
    {
        if (empty($this->list)) {
            throw  new \InvalidArgumentException('Route path not found');
        }

        if (!array_key_exists($this->current_pos, $this->list)) {
            throw  new \InvalidArgumentException('Station not found');
        }

        return $this->list[$this->current_pos];
    }

    /**
     * @return void
     */
    public function nextStation():void
    {
        if ($this->isRouteEnd()) {
            return;
        }

        $this->current_pos++;
    }

    /**
     * @return bool
     */
    public function isRouteEnd():bool
    {
        return count($this->list) - 1 === $this->current_pos;
    }

}